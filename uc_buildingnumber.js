
/**
 * @file
 * Add autofill address functionality to shipment forms.
 */

function _uc_buildingnumber_set_fields(type, address_str){
  eval('var address = ' + address_str + ';');
  var temp = type + '-' + type;
  if(address.housenumber){
    $('#edit-panes-' + temp + '-housenumber').val(address.housenumber);
  }
  else{
    $('#edit-panes-' + temp + '-housenumber').val('');
  }
  if(address.housenumber_addition){
    $('#edit-panes-' + temp + '-housenumber-addition').val('');
  }
  else{
    $('#edit-panes-' + temp + '-housenumber-addition').val('');
  }
}

$(document).ready(function () { 
  if(Drupal.settings.ucBuildingnumber.displayInline){
    $('#edit-panes-delivery-delivery-housenumber-wrapper').parents('tr').find('.field-label').html('').parent().addClass('cleared-row');
    $('#edit-panes-delivery-delivery-housenumber-addition-wrapper').parents('tr').find('.field-label').html('').parent().addClass('cleared-row');
    $('#edit-panes-billing-billing-housenumber-wrapper').parents('tr').find('.field-label').html('').parent().addClass('cleared-row');
    $('#edit-panes-billing-billing-housenumber-addition-wrapper').parents('tr').find('.field-label').html('').parent().addClass('cleared-row');
  }
  
  $('#edit-panes-delivery-delivery-address-select').change(function(){
    var address_str = $(this).val();
	if (address_str == '0')
      return;
    _uc_buildingnumber_set_fields('delivery', address_str);
  });
  
  $('#edit-panes-billing-billing-address-select').change(function(){
    var address_str = $(this).val();
	if (address_str == '0')
      return;
    _uc_buildingnumber_set_fields('billing', address_str);
  });
  
});